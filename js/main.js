$(document).ready(function(){

    $('.review').slick({
        dots: false,
        infinite: true,
        autoplay: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<span class="slide-nav prev"></span>',
        nextArrow: '<span class="slide-nav next"></span>',
        responsive: [
            {
                breakpoint: 1170,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $(".btn-modal").fancybox({
        'padding'    : 0
    });

    /*

    $('.b04').viewportChecker({
        callbackFunction: function(elem, action){
            $('.b04').addClass('scroll');
            $(window).on('scroll',function(){
                var scrollCoef = 0.003;
                $('.b04_pattern').animate({opacity:$(window).scrollTop() * scrollCoef},3000);
            });
        }
    });

    $('.b07').viewportChecker({
        callbackFunction: function(elem, action){
            $('.b07').addClass('scroll');
            $(window).on('scroll',function(){
                var scrollCoef = 0.003;
                $('.b07_pattern').animate({opacity:$(window).scrollTop() * scrollCoef},3000);
            });
        }
    });

    */
});
